#!/usr/bin/env python
#
# Get Users from an existing zabbix server and synchronise to a secondary
# Andy Fry <Andy.Fry@nec.com.au>
#
# Calls Zabbix API using pyzabbix (https://github.com/lukecyca/pyzabbix)
#

from pyzabbix import ZabbixAPI, ZabbixAPIException
import sys
import argparse
import yaml

#Get server details and credentials from servers.yml
with open("servers.yml", 'r') as stream:
    try:
        servers=yaml.load(stream)
    except yaml.YAMLError as exc:
        print(exc)

#Added to ignore the SSL certificate warnings
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

primary_username=servers['zabbix-sync::primary::username']
primary_password=servers['zabbix-sync::primary::password']
primary_url=servers['zabbix-sync::primary::url']
secondary_username=servers['zabbix-sync::secondary::username']
secondary_password=servers['zabbix-sync::secondary::password']
secondary_url=servers['zabbix-sync::secondary::url']


# Connect to Primary Zabbix server
pz=ZabbixAPI(server=primary_url)
pz.session.verify=False
pz.login(user=primary_username, password=primary_password)

sz=ZabbixAPI(server=secondary_url)
sz.session.verify=False
sz.login(user=secondary_username, password=secondary_password)

# Get Users
for puser in pz.user.get(output="extend",
                  search={ "alias": [ "guest", "Admin" ] },
                  excludeSearch='on',
                  sortfield='userid',
                  sortorder='ASC'):
    print(puser['userid'],puser['name'],puser['surname'],puser['alias'])
    suser=sz.user.get(output="extend",
                  search={ "alias": puser['alias'] })
    if suser:
        print "No need to create this one"
    else:
        output=["User ", puser['alias'], " needs to be created"]
        print "".join(output)
        pusergrps=pz.usergroup.get(output="usergrpid",
                   userids=puser['userid'])
        if pusergrps:
            try:
                cuser=sz.user.create( alias=puser['alias'],
                              usrgrps=pusergrps,
                              passwd="Password123")
            except ZabbixAPIException as e:
                print(e)
