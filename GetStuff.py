#!/usr/bin/env python
#
# Get Command Line specified stuff from specified server
# Andy Fry <Andy.Fry@nec.com.au>
#
# Calls Zabbix API using pyzabbix (https://github.com/lukecyca/pyzabbix)
#

from pyzabbix import ZabbixAPI
import sys
import argparse
import yaml

#Get server details and credentials from servers.yml
with open("servers.yml", 'r') as stream:
    try:
        servers=yaml.load(stream)
    except yaml.YAMLError as exc:
        print(exc)

#Added to ignore the SSL certificate warnings
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


#Get Server and item from command line
parser = argparse.ArgumentParser()
parser.add_argument('-s','--server', help="Server primary or secondary 1 or 2", required=True)
parser.add_argument('-o','--object', help="Object to retrieve", required=True)
args = parser.parse_args()
object=args.object
server=args.server

if server == 'secondary':
    username=servers['zabbix-sync::secondary::username']
    password=servers['zabbix-sync::secondary::password']
    url=servers['zabbix-sync::secondary::url']
else:
    username=servers['zabbix-sync::primary::username']
    password=servers['zabbix-sync::primary::password']
    url=servers['zabbix-sync::primary::url']

# Connect to Zabbix server
z=ZabbixAPI(server=url)
z.session.verify=False
z.login(user=username, password=password)

# Get Stuff
getstatement=['z.', object, '.get(output="extend", sortfield="name")']
getstatement="".join(getstatement)
for output in eval(getstatement):
    print(output)
