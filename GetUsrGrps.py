#!/usr/bin/env python
#
# Get User groups from primary server
# Andy Fry <Andy.Fry@nec.com.au>
#
# Calls Zabbix API using pyzabbix (https://github.com/lukecyca/pyzabbix)
#

from pyzabbix import ZabbixAPI
import sys
import argparse
import yaml

#Get server details and credentials from servers.yml
with open("servers.yml", 'r') as stream:
    try:
        servers=yaml.load(stream)
    except yaml.YAMLError as exc:
        print(exc)

#Added to ignore the SSL certificate warnings
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


primary_username=servers['zabbix-sync::primary::username']
primary_password=servers['zabbix-sync::primary::password']
primary_url=servers['zabbix-sync::primary::url']

# Connect to Zabbix server
z=ZabbixAPI(server=primary_url)
z.session.verify=False
z.login(user=primary_username, password=primary_password)

# Get Users
for user in z.user.get(output="extend",
#                  search={ "surname": 'Administrator', "alias": 'guest' },
                  search={ "alias": [ "guest", "Admin" ] },
                  excludeSearch='on',
                  sortfield='userid',
                  sortorder='ASC'):
    print(user['userid'],user['alias'])
    for usergrp in z.usergroup.get(output="extend",
                     userids=user['userid']):
        print(usergrp)
