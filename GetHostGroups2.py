#!/usr/bin/env python
#
# Get Host Groups groups from secondary server
# Andy Fry <Andy.Fry@nec.com.au>
#
# Calls Zabbix API using pyzabbix (https://github.com/lukecyca/pyzabbix)
#

from pyzabbix import ZabbixAPI
import sys
import argparse
import yaml

#Get server details and credentials from servers.yml
with open("servers.yml", 'r') as stream:
    try:
        servers=yaml.load(stream)
    except yaml.YAMLError as exc:
        print(exc)

#Added to ignore the SSL certificate warnings
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


secondary_username=servers['zabbix-sync::secondary::username']
secondary_password=servers['zabbix-sync::secondary::password']
secondary_url=servers['zabbix-sync::secondary::url']

# Connect to Zabbix server
z=ZabbixAPI(server=secondary_url)
z.session.verify=False
z.login(user=secondary_username, password=secondary_password)

# Get Users
for hostgroup in z.hostgroup.get(output="extend",
                  sortfield='groupid',
                  sortorder='ASC'):
    print(hostgroup)
    #for usergrp in z.usergroup.get(output="extend",
    #                 userids=user['userid']):
    #    print(usergrp)
