# zabbix-sync

[![zabbix-sync](https://assets.gitlab-static.net/uploads/-/system/project/avatar/5669789/zabbix-sync-logo.png)](https://gitlab.com/andyfry/zabbix-sync)

Synchronise 2 zabbix servers using the Zabbix API.

Credit to Luke Cyca for providing a pretty decent API module which this project relies on https://github.com/lukecyca/pyzabbix

Just starting out but this seems like a pretty good way of providing an active backup server in a distributed monitoring model

## Zabbix Architecture
You'll find architecture diagrams in the images directory.
At a high level the infrastructure consists of thre layers:
- Zabbix Servers
- Zabbix Proxies(Active)
- Zabbix Agents(Active)

#### Data Flow
- All active agents will communicate with both proxy servers
- Each proxy will only communicate with a single server

Hosts will not be added manually but auto registered using Actions. These actions are the key to the whole thing and actions created on the primary zabbix server will be replicated to the secondary. In theory it should be possible to monitor all the gaents using either the primary or secondary server. The values may not be exactly in sync but should be close enough to make no difference.

### Help
Feel free to add comments or ideas about how this could go.

## Configuration
Config file servers.yml defines primary and secondary urls and credentials. See servers.yml.example

