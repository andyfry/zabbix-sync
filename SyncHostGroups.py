#!/usr/bin/env python
#
# Get HostGroups from an existing zabbix server and synchronise to a secondary
# Andy Fry <Andy.Fry@nec.com.au>
#
# Calls Zabbix API using pyzabbix (https://github.com/lukecyca/pyzabbix)
#

from pyzabbix import ZabbixAPI, ZabbixAPIException
import sys
import argparse
import yaml

#Get server details and credentials from servers.yml
with open("servers.yml", 'r') as stream:
    try:
        servers=yaml.load(stream)
    except yaml.YAMLError as exc:
        print(exc)

#Added to ignore the SSL certificate warnings
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

primary_username=servers['zabbix-sync::primary::username']
primary_password=servers['zabbix-sync::primary::password']
primary_url=servers['zabbix-sync::primary::url']
secondary_username=servers['zabbix-sync::secondary::username']
secondary_password=servers['zabbix-sync::secondary::password']
secondary_url=servers['zabbix-sync::secondary::url']


# Connect to Primary Zabbix server
pz=ZabbixAPI(server=primary_url)
pz.session.verify=False
pz.login(user=primary_username, password=primary_password)

sz=ZabbixAPI(server=secondary_url)
sz.session.verify=False
sz.login(user=secondary_username, password=secondary_password)

# Get Users
for phostgroup in pz.hostgroup.get(output="extend",
                  sortfield='groupid',
                  sortorder='ASC'):
    print(phostgroup['groupid'],phostgroup['name'])
    shostgroup=sz.hostgroup.get(output="extend",
                  search={ "name": phostgroup['name'] })
    if shostgroup:
        print "No need to create this one"
    else:
        output=["HostGroup ", phostgroup['name'], " needs to be created"]
        print "".join(output)
        try:
            chostgroup=sz.hostgroup.create( name=phostgroup['name'])
        except ZabbixAPIException as exc:
            print(exc)
