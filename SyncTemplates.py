#!/usr/bin/env python
#
# Get Templates from primary server
# Andy Fry <Andy.Fry@nec.com.au>
#
# Calls Zabbix API using pyzabbix (https://github.com/lukecyca/pyzabbix)
#

from pyzabbix import ZabbixAPI, ZabbixAPIException
import sys
import argparse
import yaml

#Get server details and credentials from servers.yml
with open("servers.yml", 'r') as stream:
    try:
        servers=yaml.load(stream)
    except yaml.YAMLError as exc:
        print(exc)

#Added to ignore the SSL certificate warnings
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

primary_username=servers['zabbix-sync::primary::username']
primary_password=servers['zabbix-sync::primary::password']
primary_url=servers['zabbix-sync::primary::url']
secondary_username=servers['zabbix-sync::secondary::username']
secondary_password=servers['zabbix-sync::secondary::password']
secondary_url=servers['zabbix-sync::secondary::url']

# Connect to Zabbix server
pz=ZabbixAPI(server=primary_url)
pz.session.verify=False
pz.login(user=primary_username, password=primary_password)

sz=ZabbixAPI(server=secondary_url)
sz.session.verify=False
sz.login(user=secondary_username, password=secondary_password)

#Define the template rules
rules = {
    'applications': {
        'createMissing': True,
    },
    'discoveryRules': {
        'createMissing': True,
        'updateExisting': True
    },
    'graphs': {
        'createMissing': True,
        'updateExisting': True
    },
    'groups': {
        'createMissing': True
    },
    'hosts': {
        'createMissing': True,
        'updateExisting': True
    },
    'images': {
        'createMissing': True,
        'updateExisting': True
    },
    'items': {
        'createMissing': True,
        'updateExisting': True
    },
    'maps': {
        'createMissing': True,
        'updateExisting': True
    },
    'screens': {
        'createMissing': True,
        'updateExisting': True
    },
    'templateLinkage': {
        'createMissing': True,
    },
    'templates': {
        'createMissing': True,
        'updateExisting': True
    },
    'templateScreens': {
        'createMissing': True,
        'updateExisting': True
    },
    'triggers': {
        'createMissing': True,
        'updateExisting': True
    },
    'valueMaps': {
        'createMissing': True,
        'updateExisting': True
    },
}

# Get Templates
for ptemplate in pz.template.get(output="extend", sortfield="name"):
    print(ptemplate['name'], ptemplate['templateid'])
    stemplate=sz.template.get(ouput="extend",
                              search={ "name": ptemplate['name'] })
    if stemplate:
        print "No need to create this one"
    else:
        ptemplates=[ ptemplate['templateid'], ]
        export_template=pz.configuration.export(options={ "templates": ptemplates },
                                                     format="xml")
        try:
            irules={ "createmissing": "true", "updateexisting": "true" }
            itemplate=sz.confimport("xml", export_template, rules )
        except ZabbixAPIException as exc:
            print(exc)
